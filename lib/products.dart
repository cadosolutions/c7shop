
part of c7shop;

class Product extends SiteEntity, implements Content {
      One<Category> category;
      String name;

      Fields fields;

      Params params;

      double base_price;

}

class ProductOption extends SiteEntity {
      One<Product> product;
      ParamsValues values; //null
      double price_mod; //null
}
