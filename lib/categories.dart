part of c7shop;

class ProductCategory extends SiteEntity implements Content with RootedTree {

    @MaxLength(255);
    String name;

    @TextArea
    String description;

    @Reverse(#category);
    QuerySet<Product> products;

    QuerySet<Product> allProducts{
        return join(this.allChildren.products);
    }
}
