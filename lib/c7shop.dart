// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

library c7shop;

//this shall export stuff
import 'package:c7core/c7core.dart';

part 'categories.dart';
part 'products.dart';
part 'stack.dart';
part 'orders.dart';
part 'payments.dart';
part 'ratings.dart';
